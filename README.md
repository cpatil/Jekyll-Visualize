# jekyll-visualize

jekyll-visualize is simple, single page portfolio design with a fully functional lightbox. 

## Installation

Add this line to your Jekyll site's Gemfile:

```ruby
gem "jekyll-visualize"
```

And add this line to your Jekyll site:

```yaml
theme: jekyll-visualize
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-visualize

## Usage

This template is designed to display image gallery. 
 - create `images` folder in your root directory and save images in that folder.
 - To dispaly images in your site, add images in `_posts` folder as markdown file.
 	Provide image path in posts front matter

 	```yaml
		---
		title: Image 01
		full_image: bower_components/visualize/images/fulls/01.jpg
		thumbs: bower_components/visualize/images/thumbs/01.jpg
		---
 	```
 	
## Contributing

Issue reports, feature/enhancement requests and merge requests are welcome on GitLab.

## Development

To set up your environment to develop this theme, run `bundle install`.

You theme is setup just like a normal Jelyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`, and `_sass` tracked with Git will be released.

## License

The theme is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

